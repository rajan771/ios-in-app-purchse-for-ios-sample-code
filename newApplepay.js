import React, {useEffect, useState} from 'react';
import {
  Text,
  Alert,
  View,
  SafeAreaView,
  Button,
  StyleSheet,
  Platform,
} from 'react-native';
import IAP from 'react-native-iap';

const items = Platform.select({
  ios: ['com.fitindiannew_30_days'], // items
  android: [],
});
let purchaseUdateListener;
let purchaseErrorListener;
export default function Payment() {
  const [purchased, setPurchase] = useState(flase);
  const [products, setProducts] = useState({});
  const [checking, setChecking] = useState(true);


  const validate = async receipt => {
    setChecking(true);
    const receiptBody = {
      'receipt-data': receipt,
      password: 'b9ee9a2dce5b001c44b31ff9f54',  //your password
    };
const deliveryReceipt =await fetch("http://localhost.com",{
    headers:{'Content-type':'application/json'},method:'POST',
body:JSON.stringify({data:receiptBody})

}).then((res)=>{
    res.json().then((r)=>{
        console.log(r);
        if(r.result.isExpired){
            Alert.alert("expried","plan has benn expried")
        }else{
            setPurchase(true)
        }
        setChecking(false)
    }).catch(()=>{

    })
})

    const result = await IAP.validateReceiptIos(receiptBody, true)
      .catch(() => {})
      .then(receipt => {
        try {
          console.log(receipt);
          const renewalHistory = receipt.latest_expired_receipt_info;
          const expiration =
            renewalHistory[renewalHistory.length - 1].expires_date_ms;
          let expired = Date.now() > expiration;
          if (!expired) {
            setPurchase(true);
          } else {Alert.alert('purch is expried');
          setChecking(flase)
        }
        } catch (error) {}
      });
  };
  useEffect(() => {
    IAP.initConnection()
      .catch(() => {
        console.log('error to connect');
      })
      .then(() => {
        console.log('connected sucessfully');
        IAP.getSubscriptions(items)
          .catch(() => {
            console.log('error finding purchase');
          })
          .then(res => {
            console.log('datta', res);
            setProducts(res);
          });
        IAP.getPurchaseHistory()
          .catch(() => {
              setChecking(flase)
          })
          .then((res) => {
            const receipt = res[res.length - 1].transactionReceipt;
            if (receipt) {
              validate(receipt);
              console.log(receipt);
            }
          });
      });
purchaseErrorListener =IAP.purchaseErrorListener((error)=>{
    if(console.error(['responseCode']=== "2")){
        // user cancel
    }else {
        ("error","error wit purchase" + error["code"]) 
    }
})

    purchaseUdateListener = IAP.purchaseUpdatedListener(purchase => {
      //   const receipt = purchase.transactionReceipt;
      //   console.log(receipt);
      //   setPurchase(true);
      try{
          const receipt =purchase.transactionReceipt;
          if(receipt){
              validate(receipt)
          }
      }catch{

      }
    });
  }, []);

if(checking){
    return (
        <View style={styles.container}>
          <Text style={styles.text}>previues loading..</Text>
        </View>
      );
} else {
    if (purchased) {
        <View style={styles.container}>
          <Text style={styles.text}>welcome yor are purchsed</Text>
        </View>;
      } else {
        if (products.length > 0) {
          return (
            <View style={styles.container}>
              <Text style={styles.text}>{products[0]['title']}</Text>
              <Text>1 Month {products[0]['localizedPrice']}</Text>
              <View style={{height: 4, width: 50, backgroundColor: 'white'}} />
              <View
                style={{
                  backgroundColor: 'rgba(150,150,150,0.25)',
                  borderRadius: 10,
                }}>
                <Text style={styles.text}>Features</Text>
    
                <Text style={styles.smallext}>
                  30Days plan {'\n'}
                  free diet
                </Text>
              </View>
              <Button
                title="purchase"
                onPress={() => {
                  console.log('pressed');
                  IAP.requestSubscription(products[0]['productId']);
                }}></Button>
            </View>
          );
        } else {
          return (
            <View style={styles.container}>
              <Text style={styles.text}>loading..</Text>
            </View>
          );
        }
      }
}

 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColour: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    fontSize: 30,
    paddingBottom: 5,
  },
  smallext: {
    color: 'white',
    fontSize: 15,
  },
});
